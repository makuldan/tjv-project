package com.food_service.server.domain;

import com.food_service.server.dao.EntityNotFoundEXception;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity(name = "restaurant")
public class Restaurant {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name="name", nullable = false)
    private String name;
    private RestaurantType type;
    @OneToMany(mappedBy = "restaurant", cascade = CascadeType.REMOVE)
    private List<Order> orderList;

    @OneToMany(mappedBy = "restaurant", cascade = CascadeType.REMOVE)
    private List<MenuItem> items;

    public enum RestaurantType {
        PIZZA, BURGER, SUSHI, MEXICAN
    }
    public Restaurant() {
        /**
         * needed by jpa
         */
        this.items = new ArrayList<>();
        this.orderList = new ArrayList<>();
    }

    public Restaurant(long id, String name, RestaurantType type, List<Order> orderList, List<MenuItem> items) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.orderList = orderList;
        this.items = items;
    }

    public Restaurant(long id, String name, RestaurantType type) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.orderList = new ArrayList<>();
        this.items = new ArrayList<>();
    }

    public void addOrder(Order order) {
        orderList.add(order);
    }

    public void addOrders(Collection<Order> orders) {
        orderList.addAll(orders);
    }

    public void deleteOrder(Order order) throws EntityNotFoundEXception {
        if (orderList.remove(order) == false)
            throw new EntityNotFoundEXception("Order to delete was not found, id is " + order.getId());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RestaurantType getType() {
        return type;
    }

    public void setType(RestaurantType type) {
        this.type = type;
    }

    public List<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }

    public List<MenuItem> getItems() {
        return items;
    }

    public void addItem(MenuItem item) {
        items.add(item);
    }

    public void setItems(List<MenuItem> items) {
        this.items = items;
    }
}
