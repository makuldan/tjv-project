package com.food_service.server.domain;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity(name = "tbl_order")
public class Order {

    public enum Status {
        AWAITING_PAYMENT,
        COMPLETED,
        DELIVERED,
        CANCELLED
    }
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name="order_id")
    private Long id;

    @ManyToOne
    private Customer customer;
    @ManyToOne
    private Restaurant restaurant;
    @Column(nullable = true)
    private Status status;
    @Column(nullable = true)
    private LocalDateTime orderDate;

    @ManyToMany
    @JoinTable (name = "order_item",
            joinColumns = @JoinColumn(name = "order_id"),
            inverseJoinColumns = @JoinColumn(name = "item_id"))
    private List<MenuItem> items;

    public Order() {
        /**
         * needed by jpa
         */
    }

    public Order(long orderId, Customer customer, Restaurant restaurant, Status status, LocalDateTime orderDate,
                 List<MenuItem> items) {
        this.id = orderId;
        this.customer = customer;
        this.restaurant = restaurant;
        this.status = status;
        this.orderDate = orderDate;
        this.items = items;
    }

    public Order(long orderId, Customer customer, Restaurant restaurant, List<MenuItem> items) {
        this.id = orderId;
        this.customer = customer;
        this.restaurant = restaurant;
        this.items = items;
        this.status = Status.AWAITING_PAYMENT;
        this.orderDate = LocalDateTime.now();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object o) {
        /**
         * used in list.remove method
         * compare by order_id
         */
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id;
}

    public Order changeStatus(Status status) {
        ///if () ...
        this.status = status;
        return this;
    }

    public int totalPrice() {
        int price = 0;
//        for (MenuItem item : items) {
//            price += item.getPrice();
//        }
        return price;
    }


    public long getId() {
        return id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDateTime orderDate) {
        this.orderDate = orderDate;
    }

    public List<MenuItem> getItems() {
        return items;
    }

    public void setItems(List<MenuItem> items) {
        this.items = items;
    }

    public Status getStatus() {
        return status;
    }
}