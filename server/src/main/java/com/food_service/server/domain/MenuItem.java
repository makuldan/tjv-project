package com.food_service.server.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name="item")
public class MenuItem {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name="item_id")
    private Integer id;

    @ManyToOne
    private Restaurant restaurant;
    @Column(nullable=false)
    private String name;
    @Column(nullable=false)
    private int price;

    public MenuItem(int id, Restaurant restaurant, String name, int price) {
        this.id = id;
        this.restaurant = restaurant;
        this.name = name;
        this.price = price;
    }

    public MenuItem() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }
}
