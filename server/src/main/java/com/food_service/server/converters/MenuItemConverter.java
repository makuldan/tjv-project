package com.food_service.server.converters;

import com.food_service.server.domain.MenuItem;
import com.food_service.server.domain.Restaurant;
import com.food_service.server.dto.MenuItemDto;

public class MenuItemConverter {
    public static MenuItemDto toDto(MenuItem item) {
        return new MenuItemDto(item.getId(),
                               item.getRestaurant().getId(),
                               item.getName(),
                               item.getPrice());
    }

    public static MenuItem fromDto(MenuItemDto dto, Restaurant restaurant) {
        return new MenuItem(dto.getId(),
                            restaurant,
                            dto.getName(),
                            dto.getPrice());
    }
}
