package com.food_service.server.converters;

import com.food_service.server.domain.Customer;
import com.food_service.server.domain.MenuItem;
import com.food_service.server.domain.Order;
import com.food_service.server.domain.Restaurant;
import com.food_service.server.dto.OrderDto;

import java.util.ArrayList;
import java.util.List;

public class OrderConverter {
    public static OrderDto toDto(Order order) {
        List<Integer> ids = new ArrayList<>();
        order.getItems().forEach(menuItem -> ids.add(menuItem.getId()));
        /*
         * order dto does not have list of menu items
         * instead it has list of ids of those items
         */
        return new OrderDto(order.getId(), order.getCustomer().getId(),
                order.getRestaurant().getId(), order.getStatus(), order.getOrderDate(), ids);
    }

    public static Order fromDto(OrderDto dto, Customer customer, Restaurant restaurant, List<MenuItem> items) {
        return new Order(dto.getId(), customer,
                restaurant, dto.getStatus(), dto.getOrderDate(), items);
    }
}
