package com.food_service.server.converters;

import com.food_service.server.domain.MenuItem;
import com.food_service.server.domain.Order;
import com.food_service.server.domain.Restaurant;
import com.food_service.server.dto.RestaurantDto;

import java.util.ArrayList;
import java.util.List;

public class RestaurantConverter {
    public static Restaurant fromDto(RestaurantDto dto, List<Order> orders, List<MenuItem> items) {
        return new Restaurant(dto.getId(), dto.getName(), dto.getType(), orders, items);
    }

    public static RestaurantDto toDto(Restaurant restaurant) {
        List<Long> orderIds = new ArrayList<>();
        restaurant.getOrderList().forEach(order ->
            orderIds.add(order.getId()));
        List<Integer> itemIds = new ArrayList<>();
        restaurant.getItems().forEach(item ->
                itemIds.add(item.getId()));
        return new RestaurantDto(restaurant.getId(),
                restaurant.getName(), restaurant.getType(), orderIds, itemIds);
    }
}
