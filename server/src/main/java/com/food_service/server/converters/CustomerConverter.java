package com.food_service.server.converters;

import com.food_service.server.domain.Customer;
import com.food_service.server.domain.Order;
import com.food_service.server.dto.CustomerDto;

import java.util.ArrayList;
import java.util.List;

public class CustomerConverter {
    public static CustomerDto customerToDto(Customer customer) {
        List<Long> orderIds = new ArrayList<>();
        customer.getOrders().forEach(order -> orderIds.add(order.getId()));
        ///customer dto does not have list of orders themselves
        ///instead it has list of ids
        return new CustomerDto(customer.getId(), customer.getName(),
                customer.getAddress(), customer.getEmail(), customer.getNumber(), orderIds);
    }

    public static Customer dtoToCustomer(CustomerDto dto, List<Order> orders) {
        return new Customer (dto.getId(), dto.getName(), dto.getAddress(), dto.getEmail(),
                            dto.getNumber(), orders);
    }
}
