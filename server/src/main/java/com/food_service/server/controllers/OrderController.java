package com.food_service.server.controllers;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.food_service.server.business.CustomerService;
import com.food_service.server.business.MenuItemService;
import com.food_service.server.business.OrderService;
import com.food_service.server.business.RestaurantService;
import com.food_service.server.converters.CustomerConverter;
import com.food_service.server.converters.OrderConverter;
import com.food_service.server.dao.EntityNotFoundEXception;
import com.food_service.server.domain.Customer;
import com.food_service.server.domain.Order;
import com.food_service.server.dto.CustomerDto;
import com.food_service.server.dto.OrderDto;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import org.hibernate.PropertyValueException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;


import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrderController {

    private final OrderService orderService;
    private final CustomerService customerService;
    private final RestaurantService restaurantService;
    private final MenuItemService menuItemService;

    public OrderController(OrderService orderService,
                           CustomerService customerService,
                           RestaurantService restaurantService,
                           MenuItemService menuItemService) {
        this.orderService = orderService;
        this.customerService = customerService;
        this.restaurantService = restaurantService;
        this.menuItemService = menuItemService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<OrderDto> getOrders() {
        return orderService.getOrderDtos();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public OrderDto getOrder(@PathVariable long id) throws EntityNotFoundEXception {
        return OrderConverter.toDto(orderService.readById(id));
    }

    @GetMapping("/by/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Collection<OrderDto> getOrdersBy(@PathVariable long id) throws EntityNotFoundEXception {
        var orders = orderService.getAllBy(customerService.readById(id));
        return orderService.getOrderDtos(orders);
    }

    @GetMapping("/byFrom/{idCust}/{idRest}")
    @ResponseStatus(HttpStatus.OK)
    public Collection<OrderDto> getAllByFrom
    (@PathVariable long idCust, @PathVariable long idRest) throws EntityNotFoundEXception {
        return orderService.getOrderDtos(
                orderService.getAllByFrom(
                        customerService.readById(idCust),
                        restaurantService.readById(idRest))
        );
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public OrderDto createOrder
    (@RequestBody OrderDto dto) throws EntityNotFoundEXception, PropertyValueException {
        Order order = OrderConverter.fromDto(dto, customerService.readById(dto.getCustomerId()),
                                                  restaurantService.readById(dto.getRestaurantId()),
                                                  menuItemService.getItems(dto.getItemIds()));
        order.setOrderDate(LocalDateTime.now());
        return OrderConverter.toDto(orderService.create(order));
    }

    /*
        * does not work: some entity can not work with LocalDateTime class
        * did not test more ((
        * toDo:          change LocalDateTime to another class or
                       add some module (spring recommendation)
     */
    ///for info on json-patch, look http://jsonpatch.com/
    @PatchMapping(path = "/{id}", consumes = "application/json-patch+json")
    @Transactional
    @ResponseStatus(HttpStatus.OK)
    public OrderDto updateOrder
    (@PathVariable long id, @RequestBody JsonPatch patch) throws EntityNotFoundEXception, JsonPatchException, JsonProcessingException {
        Order order = orderService.readById(id);
        OrderDto orderDto = OrderConverter.toDto(order);
        OrderDto orderDtoPatched = applyPatchToOrder(patch, orderDto);
        Order orderPatched = OrderConverter.fromDto(orderDtoPatched,
                customerService.readById(orderDtoPatched.getCustomerId()),
                restaurantService.readById(orderDtoPatched.getRestaurantId()),
                menuItemService.getItems(orderDtoPatched.getItemIds())
        );

        return OrderConverter.toDto(orderService.update(orderPatched));
    }


    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteOrder(@PathVariable long id) throws EntityNotFoundEXception {
        orderService.delete(id);
    }

    private OrderDto applyPatchToOrder(JsonPatch patch, OrderDto target) throws JsonPatchException, JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode patched = patch.apply(objectMapper.convertValue(target, JsonNode.class));
        return objectMapper.treeToValue(patched, OrderDto.class);
    }
}
