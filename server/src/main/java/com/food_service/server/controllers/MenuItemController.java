package com.food_service.server.controllers;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.food_service.server.business.MenuItemService;
import com.food_service.server.business.RestaurantService;
import com.food_service.server.converters.CustomerConverter;
import com.food_service.server.converters.MenuItemConverter;
import com.food_service.server.dao.EntityNotFoundEXception;
import com.food_service.server.domain.Customer;
import com.food_service.server.domain.MenuItem;
import com.food_service.server.dto.CustomerDto;
import com.food_service.server.dto.MenuItemDto;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import org.hibernate.PropertyValueException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping("/items")
public class MenuItemController {

    private final RestaurantService restaurantService;
    private final MenuItemService menuItemService;

    @Autowired
    public MenuItemController(RestaurantService restaurantService, MenuItemService menuItemService) {
        this.restaurantService = restaurantService;
        this.menuItemService = menuItemService;
    }


    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<MenuItemDto> getItems() {
        return menuItemService.getItemDtos();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public MenuItemDto getItem(@PathVariable int id) throws EntityNotFoundEXception {
        return MenuItemConverter.toDto(menuItemService.readById(id));
    }

    @PostMapping
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    public MenuItemDto createItem
    (@RequestBody MenuItemDto dto) throws EntityNotFoundEXception, PropertyValueException {
        MenuItem item = MenuItemConverter.fromDto
                (dto, restaurantService.readById(dto.getRestaurantId()));
        restaurantService.readById(dto.getRestaurantId()).getItems().add(item);
        return MenuItemConverter.toDto(menuItemService.create(item));
    }

    ///for info on json-patch, look http://jsonpatch.com/
    @PatchMapping(path = "/{id}", consumes = "application/json-patch+json")
    @Transactional
    @ResponseStatus(HttpStatus.OK)
    public MenuItemDto updateItem
    (@PathVariable int id, @RequestBody JsonPatch patch) throws EntityNotFoundEXception, JsonPatchException, JsonProcessingException {
        MenuItem item = menuItemService.readById(id);
        MenuItemDto itemDto = MenuItemConverter.toDto(item);
        MenuItemDto itemDtoPatched = applyPatchToItem(patch, itemDto);
        MenuItem itemPatched = MenuItemConverter.fromDto(
                itemDtoPatched, restaurantService.readById(itemDtoPatched.getRestaurantId())
        );

        return MenuItemConverter.toDto(menuItemService.update(itemPatched));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteItem(@PathVariable int id) throws EntityNotFoundEXception {
        menuItemService.delete(id);
    }

    private MenuItemDto applyPatchToItem(JsonPatch patch, MenuItemDto target) throws JsonPatchException, JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode patched = patch.apply(objectMapper.convertValue(target, JsonNode.class));
        return objectMapper.treeToValue(patched, MenuItemDto.class);
    }
}
