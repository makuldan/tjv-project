package com.food_service.server.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.food_service.server.business.CustomerService;
import com.food_service.server.business.OrderService;
import com.food_service.server.converters.CustomerConverter;
import com.food_service.server.dao.EntityNotFoundEXception;
import com.food_service.server.domain.Customer;
import com.food_service.server.dto.CustomerDto;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import org.hibernate.PropertyValueException;
import org.springframework.http.HttpStatus;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/customers")
public class CustomerController {

    private final CustomerService customerService;
    private final OrderService orderService;

    public CustomerController(CustomerService customerService, OrderService orderService) {
        this.customerService = customerService;
        this.orderService = orderService;
    }

    @GetMapping
    public List<CustomerDto> getCustomers() {
        return customerService.getCustomerDtos();
    }

    @GetMapping("/{id}")
    public CustomerDto readById(@PathVariable long id) throws EntityNotFoundEXception {
        return CustomerConverter.customerToDto(customerService.readById(id));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CustomerDto createCustomer
    (@RequestBody CustomerDto dto) throws EntityNotFoundEXception, PropertyValueException {
        return CustomerConverter.customerToDto(
                customerService.create(
                CustomerConverter.dtoToCustomer(
                        dto,
                        orderService.getOrders(dto.getOrderIds()))
                )
        );
    }


    ///for info on json-patch, look http://jsonpatch.com/
    @PatchMapping(path = "/{id}", consumes = "application/json-patch+json")
    @Transactional
    @ResponseStatus(HttpStatus.OK)
    public CustomerDto updateCustomer
    (@PathVariable long id, @RequestBody JsonPatch patch) throws EntityNotFoundEXception, JsonPatchException, JsonProcessingException {
        Customer customer = customerService.readById(id);
        CustomerDto customerDto = CustomerConverter.customerToDto(customer);
        CustomerDto customerDtoPatched = applyPatchToCustomer(patch, customerDto);
        Customer customerPatched = CustomerConverter.dtoToCustomer(customerDtoPatched,
                orderService.getOrders(customerDtoPatched.getOrderIds()));

        return CustomerConverter.customerToDto(customerService.update(customerPatched));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteCustomer(@PathVariable long id) throws EntityNotFoundEXception {
        ///should also delete all orders made by this customer
        customerService.delete(id);
    }

    private CustomerDto applyPatchToCustomer(JsonPatch patch, CustomerDto target) throws JsonPatchException, JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode patched = patch.apply(objectMapper.convertValue(target, JsonNode.class));
        return objectMapper.treeToValue(patched, CustomerDto.class);
    }
}
