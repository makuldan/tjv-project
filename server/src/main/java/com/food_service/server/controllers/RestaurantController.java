package com.food_service.server.controllers;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.food_service.server.business.MenuItemService;
import com.food_service.server.business.OrderService;
import com.food_service.server.business.RestaurantService;
import com.food_service.server.converters.MenuItemConverter;
import com.food_service.server.converters.RestaurantConverter;
import com.food_service.server.dao.EntityNotFoundEXception;
import com.food_service.server.domain.MenuItem;
import com.food_service.server.domain.Restaurant;
import com.food_service.server.dto.CustomerDto;
import com.food_service.server.dto.MenuItemDto;
import com.food_service.server.dto.RestaurantDto;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import org.hibernate.PropertyValueException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping("/restaurants")
public class RestaurantController {

    private final RestaurantService restaurantService;
    private final OrderService orderService;
    private final MenuItemService menuItemService;

    public RestaurantController(RestaurantService restaurantService, OrderService orderService, MenuItemService menuItemService){
        this.orderService = orderService;
        this.restaurantService = restaurantService;
        this.menuItemService = menuItemService;
    }

    @GetMapping
    public List<RestaurantDto> getAll() {
        return restaurantService.getRestaurantDtos();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public RestaurantDto getById(@PathVariable long id) throws EntityNotFoundEXception {
        return RestaurantConverter.toDto(restaurantService.readById(id));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RestaurantDto create
    (@RequestBody RestaurantDto dto) throws EntityNotFoundEXception, PropertyValueException {
        Restaurant created = restaurantService.create(RestaurantConverter.
                fromDto(dto,
                        orderService.getOrders(dto.getOrderIds()),
                        menuItemService.getItems(dto.getItemIds())
                ));
        return RestaurantConverter.toDto(created);
    }

    ///for info on json-patch, look http://jsonpatch.com/
    @PatchMapping(path = "/{id}", consumes = "application/json-patch+json")
    @Transactional
    @ResponseStatus(HttpStatus.OK)
    public RestaurantDto updateRestaurant
    (@PathVariable long id, @RequestBody JsonPatch patch) throws EntityNotFoundEXception, JsonPatchException, JsonProcessingException {
        Restaurant restaurant = restaurantService.readById(id);
        RestaurantDto restaurantDto = RestaurantConverter.toDto(restaurant);
        RestaurantDto restaurantDtoPatched = applyPatchToRestaurant(patch, restaurantDto);
        Restaurant restaurantPatched = RestaurantConverter.fromDto(
                restaurantDtoPatched,
                orderService.getOrders(restaurantDtoPatched.getOrderIds()),
                menuItemService.getItems(restaurantDtoPatched.getItemIds())
        );

        return RestaurantConverter.toDto(restaurantService.update(restaurantPatched));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteRestaurant(@PathVariable long id) throws EntityNotFoundEXception {
        ///should also delete all orders made by restaurant
        restaurantService.delete(id);
    }

    private RestaurantDto applyPatchToRestaurant(JsonPatch patch, RestaurantDto target) throws JsonPatchException, JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode patched = patch.apply(objectMapper.convertValue(target, JsonNode.class));
        return objectMapper.treeToValue(patched, RestaurantDto.class);
    }
}
