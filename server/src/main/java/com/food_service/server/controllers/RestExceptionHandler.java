package com.food_service.server.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.food_service.server.dao.EntityNotFoundEXception;
import com.github.fge.jsonpatch.JsonPatchException;
import org.hibernate.PropertyValueException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(EntityNotFoundEXception.class)
    protected ResponseEntity<?> handle (Exception e, WebRequest r) {
        Map<String, String> map = new HashMap<>();
        map.put("message", e.getMessage());
        map.put("status", HttpStatus.NOT_FOUND.toString());
        return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(PropertyValueException.class)
    protected ResponseEntity<?> handle1 (PropertyValueException e,WebRequest r) {
        Map<String, String> map = new HashMap<>();
        map.put("Message", "entity " + e.getEntityName() + " missing non-nullable property: " +
                e.getPropertyName());
        map.put("Status", HttpStatus.BAD_REQUEST.toString());
        return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
    }

    ///json-patch associated exceptions
    @ExceptionHandler({JsonPatchException.class, JsonProcessingException.class})
    protected ResponseEntity<?> handle2(Exception e, WebRequest r) {
        Map<String, String> map = new HashMap<>();
        map.put("message", e.getMessage());
        map.put("status", HttpStatus.BAD_REQUEST.toString());
        return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
    }
}
