package com.food_service.server.dto;

import com.food_service.server.domain.Customer;
import com.food_service.server.domain.Order;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderDto {
    private long id;
    private long customerId;
    private long restaurantId;
    private Order.Status status;
    private LocalDateTime orderDate;
    private List<Integer> itemIds;

    public OrderDto() {
        /**
         * used by spring for creating OrderDto objects
         */
        id = -1L;
        status = Order.Status.AWAITING_PAYMENT;
        itemIds = new ArrayList<>();
    }

    public OrderDto(long orderId, long customerId, long restaurantId, Order.Status status, LocalDateTime orderDate,
                    List<Integer> ids) {
        this.id = orderId;
        this.customerId = customerId;
        this.restaurantId = restaurantId;
        this.status = status;
        this.orderDate = orderDate;
        this.itemIds = ids;
    }

    public long getId() {
        return id;
    }

    public void setId(long orderId) {
        this.id = orderId;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(long restaurantId) {
        this.restaurantId = restaurantId;
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDateTime date) {
        this.orderDate = date;
    }

    public Order.Status getStatus() {
        return status;
    }

    public void setStatus(Order.Status status) {
        this.status = status;
    }

    public List<Integer> getItemIds() {
        return itemIds;
    }

    public void setItemIds(List<Integer> itemIds) {
        this.itemIds = itemIds;
    }
}
