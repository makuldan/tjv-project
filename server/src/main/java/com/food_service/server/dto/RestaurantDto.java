package com.food_service.server.dto;

import com.food_service.server.domain.Restaurant;

import java.util.ArrayList;
import java.util.List;

public class RestaurantDto {
    private long id;
    private String name;
    private Restaurant.RestaurantType type;
    private List<Long> orderIds;
    private List<Integer> itemIds;

    public RestaurantDto(long id, String name, Restaurant.RestaurantType type,
                         List<Long> orderIds, List<Integer> itemIds) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.orderIds = orderIds;
        this.itemIds = itemIds;
    }

    public RestaurantDto() {
        /**
         * used by Spring for creating RestaurantDto objects
         */
        this.orderIds = new ArrayList<>();
        this.itemIds = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Restaurant.RestaurantType getType() {
        return type;
    }

    public void setType(Restaurant.RestaurantType type) {
        this.type = type;
    }

    public List<Long> getOrderIds() {
        return orderIds;
    }

    public void setOrderIds(List<Long> orderIds) {
        this.orderIds = orderIds;
    }

    public List<Integer> getItemIds() {
        return itemIds;
    }

    public void setItemIds(List<Integer> itemIds) {
        this.itemIds = itemIds;
    }
}
