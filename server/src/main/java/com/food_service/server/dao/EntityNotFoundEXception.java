package com.food_service.server.dao;

/**
 * all services throw this exception when requested object was not found
 * or when object with given id already exists (for update and create methods)
 */
public class EntityNotFoundEXception extends Exception {
    public String message;

    public EntityNotFoundEXception() {}
    public EntityNotFoundEXception(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
