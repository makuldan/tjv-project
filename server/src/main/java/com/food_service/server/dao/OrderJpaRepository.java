package com.food_service.server.dao;

import com.food_service.server.domain.Customer;
import com.food_service.server.domain.Order;
import com.food_service.server.domain.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;




@Repository
public interface OrderJpaRepository extends JpaRepository<Order, Long> {

    /*
        wow, objects in query !!!
     */
    @Query("SELECT o FROM tbl_order o WHERE o.customer = :customer and o.restaurant = :restaurant")
    Collection<Order> findAllByCustomerAndRestaurant(@Param("customer") Customer customer,
                                    @Param("restaurant") Restaurant restaurant);

    @Query("SELECT o FROM tbl_order o WHERE o.customer = :customer")
    Collection<Order> findAllByCustomer(@Param("customer") Customer customer);
}
