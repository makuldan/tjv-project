package com.food_service.server.business;

import com.food_service.server.dao.EntityNotFoundEXception;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract class CRUDService<K,E> {

    protected JpaRepository<E, K> repository;

    protected CRUDService(JpaRepository<E,K> repository) {
        this.repository = repository;
    }

    public List<E> getAll() {
        return repository.findAll().stream().toList();
    }

    public E readById(K id) throws EntityNotFoundEXception {
        return repository.findById(id).orElseThrow(
                ()-> new EntityNotFoundEXception("Entity ID was not found")
        );
    }

    public E create(E entity) throws EntityNotFoundEXception {
        if (repository.existsById(getId(entity)))
            throw new EntityNotFoundEXception("Entity with given ID already exists");
        return repository.save(entity);
    }

    public E update(E e) throws EntityNotFoundEXception {
        if (!repository.existsById(getId(e)))
            throw new EntityNotFoundEXception("Entity with given ID does not exist");
        return repository.save(e);
    }

    public void delete(K id) throws EntityNotFoundEXception {
        if (!repository.existsById(id))
            throw new EntityNotFoundEXception("Entity ID was not found");
        repository.deleteById(id);
    }

    public boolean contains(K id) {
        return repository.existsById(id);
    }

    abstract K getId(E e);
}
