package com.food_service.server.business;

import com.food_service.server.converters.CustomerConverter;
import com.food_service.server.dao.CustomerJpaRepository;
import com.food_service.server.dao.EntityNotFoundEXception;
import com.food_service.server.domain.Customer;
import com.food_service.server.dto.CustomerDto;
import com.food_service.server.dto.OrderDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerService extends CRUDService<Long, Customer>{

    @Autowired
    CustomerService(CustomerJpaRepository repository) {
        super(repository);
    }

    public List<CustomerDto> getCustomerDtos() {
        List<CustomerDto> list = new ArrayList<>();
        repository.findAll().forEach(customer->
                list.add(CustomerConverter.customerToDto(customer))
        );
        return list;
    }

//    @Override
//    public void delete(Long id) throws EntityStateException {
//        if (!repository.existsById(id))
//            throw new EntityStateException("Customer ID was not found");
//        repository.deleteById(id);
//    }

    @Override
    Long getId(Customer customer) {
        return customer.getId();
    }

    @Override
    public void delete(Long id) throws EntityNotFoundEXception {
        if (!repository.existsById(id))
            throw new EntityNotFoundEXception("Entity ID was not found");
        repository.deleteById(id);
    }
}
