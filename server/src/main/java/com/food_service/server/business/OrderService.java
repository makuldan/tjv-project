package com.food_service.server.business;

import com.food_service.server.converters.OrderConverter;
import com.food_service.server.dao.EntityNotFoundEXception;
import com.food_service.server.dao.OrderJpaRepository;
import com.food_service.server.domain.Customer;
import com.food_service.server.domain.MenuItem;
import com.food_service.server.domain.Order;
import com.food_service.server.domain.Restaurant;
import com.food_service.server.dto.OrderDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class OrderService extends CRUDService<Long, Order>{

    @Autowired
    public OrderService(OrderJpaRepository repository) {
        super(repository);
    }

    @Override
    public Order readById(Long id) throws EntityNotFoundEXception {
        return repository.findById(id).orElseThrow(
                ()-> new EntityNotFoundEXception("Order id was not found")
        );
    }

    @Override
    public Order create(Order order) throws EntityNotFoundEXception {
        if (repository.existsById(order.getId()))
            throw new EntityNotFoundEXception("Order with given id already exists");
        for (MenuItem item : order.getItems()) {
            if (item.getRestaurant().getId() != order.getRestaurant().getId())
                throw new EntityNotFoundEXception("Order can not have items from different restaurants");
        }
        return repository.save(order);
    }

    ///get list of orders by their ids
    public List<Order> getOrders(List<Long> ids) {
        return repository.findAllById(ids);
    }

    public List<OrderDto> getOrderDtos(Collection<Order> orders) {
        List<OrderDto> list = new ArrayList<>();
        orders.forEach(order -> list.add(OrderConverter.toDto(order)));
        return list;
    }

    public List<OrderDto> getOrderDtos(){
        List<OrderDto> list = new ArrayList<>();
        repository.findAll().forEach(order->
                list.add(OrderConverter.toDto(order))
        );
        return list;
    }

    ///delete collection of orders
    public void delete(Collection<Order> orders) throws EntityNotFoundEXception {
        repository.deleteAll(orders);
    }

    ///@returns all orders made by customer
    public Collection<Order> getAllBy(Customer customer) {
        return ((OrderJpaRepository)repository).findAllByCustomer(customer);
    }

    ///@returns all orders made by customer from restaurant
    public Collection<Order> getAllByFrom(Customer customer, Restaurant restaurant){
        return ((OrderJpaRepository)repository).findAllByCustomerAndRestaurant(customer, restaurant);
    }

    @Override
    Long getId(Order order) {
        return order.getId();
    }
}
