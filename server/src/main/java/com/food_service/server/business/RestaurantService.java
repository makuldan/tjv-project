package com.food_service.server.business;

import com.food_service.server.converters.RestaurantConverter;
import com.food_service.server.dao.RestaurantJpaRepository;
import com.food_service.server.domain.Restaurant;
import com.food_service.server.dto.RestaurantDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RestaurantService extends CRUDService<Long, Restaurant> {

    @Autowired
    public RestaurantService(RestaurantJpaRepository repository) {
        super(repository);
    }

    public List<RestaurantDto> getRestaurantDtos() {
        List<RestaurantDto> restaurants = new ArrayList<>();
        getAll().forEach(restaurant -> {
            restaurants.add(RestaurantConverter.toDto(restaurant));
        });
        return restaurants;
    }

    @Override
    Long getId(Restaurant restaurant) {
        return restaurant.getId();
    }
}
