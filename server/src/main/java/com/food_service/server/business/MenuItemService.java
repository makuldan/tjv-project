package com.food_service.server.business;

import com.food_service.server.converters.MenuItemConverter;
import com.food_service.server.dao.EntityNotFoundEXception;
import com.food_service.server.dao.MenuItemJpaRepository;
import com.food_service.server.domain.MenuItem;
import com.food_service.server.dto.MenuItemDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MenuItemService extends CRUDService<Integer, MenuItem>{

    @Autowired
    public MenuItemService(MenuItemJpaRepository repository) {
        super(repository);
    }

    ///returns list of item by their ids
    public List<MenuItem> getItems(List<Integer> ids) throws EntityNotFoundEXception {
        for (int id : ids)
            if (repository.findById(id).isEmpty())
                throw new EntityNotFoundEXception("Menu Item with given ID was not found");
        return repository.findAllById(ids);
    }

    public List<MenuItemDto> getItemDtos(){
        var all = repository.findAll();
        List<MenuItemDto> dtos = new ArrayList<>();
        for (MenuItem item : all) {
            dtos.add(MenuItemConverter.toDto(item));
        }
        return dtos;
    }

    @Override
    Integer getId(MenuItem menuItem) {
        return menuItem.getId();
    }
}
