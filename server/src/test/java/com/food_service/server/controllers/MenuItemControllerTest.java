package com.food_service.server.controllers;

import com.food_service.server.business.MenuItemService;
import com.food_service.server.business.RestaurantService;
import com.food_service.server.converters.MenuItemConverter;
import com.food_service.server.dao.EntityNotFoundEXception;
import com.food_service.server.dao.MenuItemJpaRepository;
import com.food_service.server.domain.MenuItem;
import com.food_service.server.domain.Restaurant;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.NestedServletException;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(MenuItemController.class)
public class MenuItemControllerTest {

    /*
     * every menuItem has dependency on Restaurant
     */
    @MockBean
    RestaurantService restaurantService;
    @MockBean
    MenuItemService menuItemService;
    @MockBean
    MenuItemJpaRepository repository;
    @Autowired
    MockMvc mock;

    Restaurant restaurant1 = new Restaurant(1L, "test1", Restaurant.RestaurantType.BURGER);
    MenuItem item1 = new MenuItem(1, restaurant1, "item1", 100);
    MenuItem item2 = new MenuItem(2, restaurant1, "item2", 100);


    @Test
    public void getAll() throws Exception {
        ///mocking menuItemService : all items are item1, item2///---------------
        var itemDtos = List.of(
                MenuItemConverter.toDto(item1),
                MenuItemConverter.toDto( item2)
        );
        Mockito.when(menuItemService.getItemDtos()).thenReturn(itemDtos);
        ///testing method///----------------------------------------------------
        mock.perform(
                get("/items").
                accept(MediaType.APPLICATION_JSON)).
                andExpect(status().isOk()).
                andExpectAll(
                    jsonPath("$", Matchers.hasSize(2)),
                    jsonPath("$[0].id", Matchers.is(item1.getId())),
                    jsonPath("$[0].name", Matchers.is(item1.getName())),
                    jsonPath("$[0].restaurantId",
                            Matchers.is((int) item1.getRestaurant().getId())),
                    jsonPath("$[0].price", Matchers.is(item1.getPrice())),
                    jsonPath("$[1].id", Matchers.is(item2.getId())),
                    jsonPath("$[1].name", Matchers.is(item2.getName())),
                    jsonPath("$[1].restaurantId",
                            Matchers.is((int) item2.getRestaurant().getId())),
                    jsonPath("$[1].price", Matchers.is(item2.getPrice()))
                );
        ///verifying that service called needed method///------------------------------
        Mockito.verify(menuItemService, Mockito.times(1)).getItemDtos();
    }

    @Test
    public void getById() throws Exception {
        ///mocking menuItemService///-------------------------------------
        Mockito.when(menuItemService.readById(1)).thenReturn(item1);
        Mockito.when(menuItemService.readById(2)).thenThrow(
                new EntityNotFoundEXception("")
        );
        ///success///-----------------------------------------------------
        mock.perform(
                get("/items/1").
                accept(MediaType.APPLICATION_JSON)).
                andExpect(status().isOk()).
                andExpectAll(
                        jsonPath("$.id", Matchers.is(item1.getId())),
                        jsonPath("$.name", Matchers.is(item1.getName())),
                        jsonPath("$.restaurantId",
                                Matchers.is((int) item1.getRestaurant().getId())),
                        jsonPath("$.price", Matchers.is(item1.getPrice())
                )
        );
        Mockito.verify(menuItemService, Mockito.times(1)).readById(1);
        ///getting non-existing item///-------------------------------------------
        mock.perform(
                get("/items/2").
                accept(MediaType.APPLICATION_JSON)).
                andExpect(status().isNotFound()
        );
        Mockito.verify(menuItemService, Mockito.times(1)).readById(1);
    }

    @Test
    public void createItem() throws Exception {
        ///creating item with restaurantId = 1 (success)///--------------------
        Mockito.when(restaurantService.readById(1L)).thenReturn(restaurant1);
        Assertions.assertThrows(
                ///controller will throw exception, because mocking service will return null
                ///since there is no way how to mock create method
                NestedServletException.class, ()->
                mock.perform(
                post("/items").
                contentType(MediaType.APPLICATION_JSON).
                content("""
                        {
                            "restaurantId" : 1,
                            "name" : "success",
                            "price" : 120
                        }"""
                       )).
                andExpect(status().isCreated()
                )
        );
        Mockito.verify(restaurantService, Mockito.times(2)).readById(1L);
        ///trying to create item with non-existing restaurant(id = 2)///-------
        Mockito.when(restaurantService.readById(2L)).thenThrow(
                new EntityNotFoundEXception()
        );
        mock.perform(
                post("/items").
                contentType(MediaType.APPLICATION_JSON).
                content("{" +
                        "\"name\": \"notSuccess\"," +
                        "\"restaurantId\": 2,"+
                        "\"price\": 200" +
                        "}"
                )).andExpect(status().isNotFound()
        );
        Mockito.verify(restaurantService, Mockito.times(1)).readById(2L);
        ///trying to create item without name (it's necessary)///-----------------------
//        mock.perform(
//                post("/items").
//                contentType(MediaType.APPLICATION_JSON).
//                content("{" +
//                        "\"price\": 200," +
//                        "\"restaurantId\": 1" +
//                        "}")
//                ).
//                andExpect(status().isBadRequest()
//        );
    }

    @Test
    public void deleteItem() throws Exception {
        ///item with id 2 does not exist///------------------------------------------
        var f = menuItemService.getClass().getMethod("delete", Integer.class);
        Mockito.when(f.invoke(menuItemService,2)).thenThrow(
                new EntityNotFoundEXception("Entity with given ID does not exist")
        );
        ///deleting item with existing id///-----------------------------------------
        mock.perform(
                delete("/items/1").
                accept(MediaType.APPLICATION_JSON)).
                andExpect(status().isOk()
        );
        Mockito.verify(menuItemService, Mockito.times(1)).delete(1);
        ///deleting item with non-existing id///-------------------------------------
        mock.perform(
                delete("/items/2").
                accept(MediaType.APPLICATION_JSON)).
                andExpect(status().isNotFound()
        );
        Mockito.verify(menuItemService, Mockito.times(1)).delete(2);
    }
}