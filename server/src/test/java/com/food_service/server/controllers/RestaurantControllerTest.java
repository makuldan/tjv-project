package com.food_service.server.controllers;

import com.food_service.server.business.MenuItemService;
import com.food_service.server.business.OrderService;
import com.food_service.server.business.RestaurantService;
import com.food_service.server.converters.RestaurantConverter;
import com.food_service.server.dao.EntityNotFoundEXception;
import com.food_service.server.domain.Restaurant;
import com.jayway.jsonpath.JsonPath;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.util.NestedServletException;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(RestaurantController.class)
public class RestaurantControllerTest {

    @Autowired
    private MockMvc mock;
    @MockBean
    RestaurantService restaurantService;
    @MockBean
    OrderService orderService;
    @MockBean
    MenuItemService menuItemService;
//
//    @InjectMocks
//    RestaurantController restaurantController;

    Restaurant restaurant1 = new Restaurant(1L, "test1", Restaurant.RestaurantType.BURGER);
    Restaurant restaurant2 = new Restaurant(2L, "test2", Restaurant.RestaurantType.PIZZA);


    @Test
    public void getAll() throws Exception {
        ///mocking restaurant service class
        var restaurants = List.of(
                RestaurantConverter.toDto(restaurant1),
                RestaurantConverter.toDto(restaurant2)
        );
        Mockito.when(restaurantService.getRestaurantDtos()).thenReturn(restaurants);

        mock.perform(
                        get("/restaurants").
                                accept("application/json")).
                andExpect(status().isOk()).
                andExpectAll(
                        jsonPath("$", Matchers.hasSize(2)),
                        jsonPath("$[0].name", Matchers.is("test1")),
                        ///json does not distinguish b/n long and int
                        jsonPath("$[0].id", Matchers.is(1)),
                        jsonPath("$[1].name", Matchers.is("test2")),
                        jsonPath("$[1].id", Matchers.is(2)),
                        jsonPath("$[0].type", Matchers.is("BURGER")),
                        jsonPath("$[1].type", Matchers.is("PIZZA"))
                );
        Mockito.verify(restaurantService,
                Mockito.times(1)).getRestaurantDtos();
    }

    @Test
    void getById() throws Exception {
        /*
         * 2 cases:
         *      restaurant with given id was found -> return this restaurant
         *      restaurant with given id was not found (error + exception)
         */
        ///mocking restaurantService//-------------------------------------
        Mockito.when(restaurantService.readById(1L)).thenReturn(restaurant1);
        Mockito.when(restaurantService.readById(2L)).thenThrow(
                new EntityNotFoundEXception("Entity ID was not found")
        );
        ///getting existing restaurant///-------------------------------------
        MockHttpServletResponse response = mock.perform(
                get("/restaurants/1").
                accept(MediaType.APPLICATION_JSON)).
                andExpect(status().isOk()).
                andExpectAll(
                        jsonPath("$.id", Matchers.is(1)),
                        jsonPath("$.name", Matchers.is("test1")),
                        jsonPath("$.type", Matchers.is("BURGER"))
                ).andReturn().getResponse();
        Assertions.assertEquals(0, ((List<Integer>)
                JsonPath.parse(response.getContentAsString()).read("$.itemIds")).size());
        Assertions.assertEquals(0, ((List<Integer>)
                JsonPath.parse(response.getContentAsString()).read("$.orderIds")).size());
        ///getting non-existing restaurant///-----------------------------------
        mock.perform(
                get("/restaurants/2").
                accept(MediaType.APPLICATION_JSON)).
                andExpect(status().isNotFound()).
                andExpect(
                        jsonPath("$.message", Matchers.is("Entity ID was not found"))
                );
        ///verifying what controller did///-------------------------------------
        Mockito.verify(restaurantService,
                Mockito.times(1)).readById(1L);
        Mockito.verify(restaurantService,
                Mockito.times(1)).readById(2L);
    }

    @Test
    void create() throws Exception {
        ///success///------------------------------------------------
        Assertions.assertThrows(
                ///controller will throw exception, because mocking service will return null
                ///since there is no way how to mock create method
                NestedServletException.class, ()->
             mock.perform(
             post("/restaurants").
             contentType(MediaType.APPLICATION_JSON).
             content("{" +
                     "\"name\": \"test1\"," +
                     "\"type\": \"PIZZA\"" +
                     "}")).
             andExpect(status().isCreated())
        );
        ///wrong json format (name is necessary)///--------------------
        mock.perform(
            post("/restaurants").
            contentType(MediaType.APPLICATION_JSON).
            content("{" +
                    "\"type\": \"BURGER\", " +
                    "}")).
            andExpect(status().isBadRequest());
        ///==========================================================
    }

    @Test
    void deleteRestaurant() throws Exception {
        /**
         * controller (service) throws EntityNotFoundException
         * that is then caught by ExceptionHandler
         * which will return NOT FOUND response
         */
        var f = restaurantService.getClass().getMethod("delete", Long.class);
        Mockito.when(f.invoke(restaurantService,2L)).
                thenThrow(
                        new EntityNotFoundEXception("Entity with given ID does not exist")
                );
        ///success///-----------------------------------------------------
        mock.perform(
                MockMvcRequestBuilders.delete("/restaurants/1").
                accept(MediaType.APPLICATION_JSON)).
             andExpect(status().isOk()
        );

        Mockito.verify(restaurantService, Mockito.times(1)).delete(1L);
        ///trying to delete non-existing restaurant///-----------------------
        mock.perform(
                MockMvcRequestBuilders.delete("/restaurants/2").
                accept(MediaType.APPLICATION_JSON)).
             andExpect(status().isNotFound()
        );

        Mockito.verify(restaurantService, Mockito.times(1)).delete(2L);
    }
}