package com.food_service.server.controllers;

import com.food_service.server.business.CustomerService;
import com.food_service.server.business.MenuItemService;
import com.food_service.server.business.OrderService;
import com.food_service.server.business.RestaurantService;
import com.food_service.server.converters.OrderConverter;
import com.food_service.server.dao.EntityNotFoundEXception;
import com.food_service.server.domain.Customer;
import com.food_service.server.domain.MenuItem;
import com.food_service.server.domain.Order;
import com.food_service.server.domain.Restaurant;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.NestedServletException;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(OrderController.class)
public class OrderControllerTest {

    @Autowired
    MockMvc mock;
    /*
     * every order is dependent on customer, restaurant and menuItem
     */
    @MockBean
    OrderService orderService;
    @MockBean
    CustomerService customerService;
    @MockBean
    MenuItemService menuItemService;
    @MockBean
    RestaurantService restaurantService;

    Customer customer1 = new Customer(1L, "testCustomer1", null, "email", null);
    Customer customer2 = new Customer(2L, "testCustomer2", null, "email", null);
    Restaurant restaurant1 = new Restaurant(2L, "testRest1", Restaurant.RestaurantType.BURGER);
    Restaurant restaurant2 = new Restaurant(3L, "testRest2", Restaurant.RestaurantType.BURGER);
    MenuItem item1 = new MenuItem(1, restaurant1, "testItem", 120);
    MenuItem item2 = new MenuItem(2, restaurant1, "testItem", 120);
    MenuItem item3 = new MenuItem(3, restaurant2, "testItem", 120);

    Order order1 = new Order(4L, customer1, restaurant1, List.of(item1, item2));
    Order order2 = new Order(5L, customer2, restaurant2, List.of(item3));

    OrderControllerTest() {
        restaurant1.addItem(item1);
        restaurant1.addItem(item2);
        restaurant2.addItem(item3);
    }

    @Test
    public void getAll() throws Exception {
        ///mocking OrderService///--------------------------------------
        var orderDtos = List.of(
                OrderConverter.toDto(order1),
                OrderConverter.toDto(order2)
        );
        Mockito.when(orderService.getOrderDtos()).thenReturn(orderDtos);
        ///testing method///---------------------------------------------
        mock.perform(
             get("/orders").
             accept(MediaType.APPLICATION_JSON)).
             andExpect(status().isOk()).
             andExpectAll(
                     jsonPath("$", hasSize(2)),
                     ///1st order
                     jsonPath("$[0].id", is((int) order1.getId())),
                     jsonPath("$[0].customerId", is((int) order1.getCustomer().getId())),
                     jsonPath("$[0].restaurantId", is((int) order1.getRestaurant().getId())),
                     jsonPath("$[0].status", is(order1.getStatus().toString())),
                     ///2nd order
                     jsonPath("$[1].id", is((int) order2.getId())),
                     jsonPath("$[1].customerId", is((int) order2.getCustomer().getId())),
                     jsonPath("$[1].restaurantId", is((int) order2.getRestaurant().getId())),
                     jsonPath("$[1].status", is(order2.getStatus().toString()))
             );
        Mockito.verify(orderService, Mockito.times(1)).getOrderDtos();
    }

    @Test
    public void getById() throws Exception {
        ///mocking OrderService class///---------------------------------------
        long nonExistingId = 123L;
        Mockito.when(orderService.readById(order1.getId())).thenReturn(order1);
        Mockito.when(orderService.readById(123L)).thenThrow(
                new EntityNotFoundEXception()
        );
        ///getting exiting order///---------------------------------------------
        mock.perform(
             get("/orders/" + order1.getId()).
             accept(MediaType.APPLICATION_JSON)).
             andExpect(status().isOk()).
             andExpectAll(
                     jsonPath("$.id", is((int) order1.getId())),
                     jsonPath("$.customerId", is((int) order1.getCustomer().getId())),
                     jsonPath("$.restaurantId", is((int) order1.getRestaurant().getId())),
                     jsonPath("$.status", is(order1.getStatus().toString()))
             );
        Mockito.verify(orderService, Mockito.times(1)).readById(order1.getId());
        ///getting non-existing order///-------------------------------------------------------------
        mock.perform(
             get("/orders/" + nonExistingId).
             accept(MediaType.APPLICATION_JSON)).
             andExpect(status().isNotFound());
        Mockito.verify(orderService, Mockito.times(1)).readById(nonExistingId);
    }

    @Test
    public void createSuccess() throws Exception {
        ///preparing///
        long customerId = customer2.getId();
        long restaurantId = restaurant1.getId();
        List<Integer> itemIds = List.of(item1.getId(), item2.getId());
        Mockito.when(customerService.readById(customerId)).thenReturn(customer2);
        Mockito.when(restaurantService.readById(restaurantId)).thenReturn(restaurant1);
        Mockito.when(menuItemService.getItems(itemIds)).thenReturn(List.of(item1,item2));
        ///testing method///-------------------------------------------------------------
        Assertions.assertThrows(
                ///controller will throw exception, because mocking service will return null
                ///since there is no way how to mock create method
                NestedServletException.class, ()->
                mock.perform(
                post("/orders").
                accept(MediaType.ALL).
                contentType(MediaType.APPLICATION_JSON).
                content(
                        "{" +
                                "\"customerId\" :" + customerId + "," +
                                "\"restaurantId\" :" + restaurantId + "," +
                                "\"itemIds\" : " +
                                    "[" +
                                        itemIds.get(0) + "," +
                                        itemIds.get(1) +
                                    "]" +
                        "}"
                )).
                andExpect(status().isCreated())
        );
        Mockito.verify(customerService, Mockito.times(1)).readById(customerId);
        Mockito.verify(restaurantService, Mockito.times(1)).readById(restaurantId);
        Mockito.verify(menuItemService, Mockito.times(1)).getItems(itemIds);
    }

    @Test
    public void createFail1() throws Exception {
        ///preparation : order has wrong customer///------------------------------
        long existingCustomerId = customer1.getId();
        long nonExistingCustomerId = 531L;
        long existingRestId = restaurant1.getId();
        long nonExistingRestId = 323L;
        int existingItemId = item1.getId();
        int nonExistingItemId = 115;
        Mockito.when(customerService.readById(existingCustomerId)).thenReturn(customer1);
        Mockito.when(restaurantService.readById(existingRestId)).thenReturn(restaurant1);
        Mockito.when(menuItemService.getItems(List.of(existingItemId))).thenReturn(List.of(item1));
        Mockito.when(customerService.readById(nonExistingCustomerId)).thenThrow(
                new EntityNotFoundEXception()
        );
        Mockito.when(restaurantService.readById(nonExistingRestId)).thenThrow(
                new EntityNotFoundEXception()
        );
        Mockito.when(menuItemService.getItems(List.of(nonExistingItemId))).thenThrow(
                new EntityNotFoundEXception()
        );
        ///testing///-------------------------------------------------------------
        mock.perform(
                post("/orders").
                        contentType(MediaType.APPLICATION_JSON).
                        content(
                                "{" +
                                        "\"customerId\" :" + nonExistingCustomerId + "," +
                                        "\"restaurantId\" :" + existingRestId + "," +
                                        "\"itemIds\" : " +
                                        "[" +
                                        existingItemId +
                                        "]" +
                                 "}"
                        )).
                andExpect(status().isNotFound());
        Mockito.verify(customerService, Mockito.times(1)).readById(nonExistingCustomerId);
        Mockito.verify(restaurantService, Mockito.never()).readById(existingRestId);
        Mockito.verify(menuItemService, Mockito.never()).getItems(List.of(existingItemId));
    }

    @Test
    public void createFail2() throws Exception {
        ///preparation: order has wrong restaurant///---------------------------------------------
        long existingCustomerId = customer1.getId();
        long nonExistingCustomerId = 531L;
        long existingRestId = restaurant1.getId();
        long nonExistingRestId = 323L;
        int existingItemId = item1.getId();
        int nonExistingItemId = 115;
        Mockito.when(customerService.readById(existingCustomerId)).thenReturn(customer1);
        Mockito.when(restaurantService.readById(existingRestId)).thenReturn(restaurant1);
        Mockito.when(menuItemService.getItems(List.of(existingItemId))).thenReturn(List.of(item1));
        Mockito.when(customerService.readById(nonExistingCustomerId)).thenThrow(
                new EntityNotFoundEXception()
        );
        Mockito.when(restaurantService.readById(nonExistingRestId)).thenThrow(
                new EntityNotFoundEXception()
        );
        Mockito.when(menuItemService.getItems(List.of(nonExistingItemId))).thenThrow(
                new EntityNotFoundEXception()
        );
        ///testing///-------------------------------------------------------------
        mock.perform(
                        post("/orders").
                                contentType(MediaType.APPLICATION_JSON).
                                content(
                                        "{" +
                                                "\"customerId\" :" + existingCustomerId + "," +
                                                "\"restaurantId\" :" + nonExistingRestId + "," +
                                                "\"itemIds\" : " +
                                                "[" +
                                                existingItemId +
                                                "]" +
                                        "}"
                                )).
                andExpect(status().isNotFound());
        Mockito.verify(customerService, Mockito.times(1)).readById(existingCustomerId);
        Mockito.verify(restaurantService, Mockito.times(1)).readById(nonExistingRestId);
        Mockito.verify(menuItemService, Mockito.never()).getItems(List.of(existingItemId));
    }

    @Test
    public void createFail3() throws Exception {
        ///preparation: order has wrong item///--------------------------------------
        long existingCustomerId = customer1.getId();
        long nonExistingCustomerId = 531L;
        long existingRestId = restaurant1.getId();
        long nonExistingRestId = 323L;
        int existingItemId = item1.getId();
        int nonExistingItemId = 115;
        Mockito.when(customerService.readById(existingCustomerId)).thenReturn(customer1);
        Mockito.when(restaurantService.readById(existingRestId)).thenReturn(restaurant1);
        Mockito.when(menuItemService.getItems(List.of(existingItemId))).thenReturn(List.of(item1));
        Mockito.when(customerService.readById(nonExistingCustomerId)).thenThrow(
                new EntityNotFoundEXception()
        );
        Mockito.when(restaurantService.readById(nonExistingRestId)).thenThrow(
                new EntityNotFoundEXception()
        );
        Mockito.when(menuItemService.getItems(List.of(nonExistingItemId))).thenThrow(
                new EntityNotFoundEXception()
        );
        ///testing///--------------------------------------------------
        mock.perform(
                        post("/orders").
                                contentType(MediaType.APPLICATION_JSON).
                                content(
                                        "{" +
                                                "\"customerId\" :" + existingCustomerId + "," +
                                                "\"restaurantId\" :" + existingRestId + "," +
                                                "\"itemIds\" : " +
                                                "[" +
                                                nonExistingItemId +
                                                "]" +
                                        "}"
                                )).
                andExpect(status().isNotFound());
        Mockito.verify(customerService, Mockito.times(1)).readById(existingCustomerId);
        Mockito.verify(restaurantService, Mockito.times(1)).readById(existingRestId);
        Mockito.verify(menuItemService, Mockito.times(1)).getItems(List.of(nonExistingItemId));
    }

    @Test
    public void deleteOrder() throws Exception {
        long existingOrderId = order1.getId();
        long nonExistingOrderId = order2.getId();
        Mockito.doThrow(new EntityNotFoundEXception()).when(orderService).delete(nonExistingOrderId);
        Mockito.doNothing(/* service deletes order and returns nothing */).when(orderService).delete(existingOrderId);
        ///deleting existing///--------------------------------------------------
        mock.perform(
                delete("/orders/" + existingOrderId)).
                andExpect(status().isOk());
        Mockito.verify(orderService, Mockito.times(1)).delete(existingOrderId);
        ///deleting non-existing order///----------------------------------------
        mock.perform(
                delete("/orders/" + nonExistingOrderId)).
                andExpect(status().isNotFound());
        Mockito.verify(orderService, Mockito.times(1)).delete(nonExistingOrderId);
    }

}