package com.food_service.server.controllers;

import com.food_service.server.business.CustomerService;
import com.food_service.server.business.OrderService;
import com.food_service.server.converters.CustomerConverter;
import com.food_service.server.dao.EntityNotFoundEXception;
import com.food_service.server.domain.Customer;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.NestedServletException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;

/**
 * test is failed if some exception was throws
 */

@WebMvcTest(CustomerController.class)
public class CustomerControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private CustomerService customerService;
    @MockBean
    private OrderService orderService;

    Customer customer1= new Customer(1L, "test1", null,"demo_email@mail.com","number");
    Customer customer2= new Customer(2L, "test2", null,null,null);

    @Test
    void getAll() throws Exception {
        //list of 2 customers to return as all customers
        var customers = List.of(
                CustomerConverter.customerToDto(customer1),
                CustomerConverter.customerToDto(customer2)
        );
        //mocking customerService//-------------------------------------------
        Mockito.when(customerService.getCustomerDtos()).thenReturn(customers);

        ///testing method//---------------------------------------------------
        mockMvc.perform(
                get("/customers").
                accept("application/json")).
                andExpect(status().isOk()).
                andExpectAll(
                        jsonPath("$", Matchers.hasSize(2)),
                        jsonPath("$[0].name", Matchers.is("test1")),
                        ///json does not distinguish b/n long and int
                        jsonPath("$[0].id", Matchers.is(1)),
                        jsonPath("$[1].name", Matchers.is("test2")),
                        jsonPath("$[1].id", Matchers.is(2))
                );
        Mockito.verify(customerService,
                Mockito.times(1)).getCustomerDtos();
    }

    @Test
    void getCustomerById() throws Exception {
        /*
         * 2 cases:
         *      customer with given id was not found (error + exception)
         *      customer with given id was found -> return this customer
         */

        ///mocking customerService//-------------------------------------
        Mockito.when(customerService.readById(1L)).thenReturn(customer1);
        Mockito.when(customerService.readById(2L)).thenThrow(
                new EntityNotFoundEXception("Entity ID was not found")
        );
        ///getting existing customer///-------------------------------------
        mockMvc.perform(
                        get("/customers/1").
                                accept(MediaType.APPLICATION_JSON)).
                andExpect(status().isOk()).
                andExpectAll(
                        jsonPath("$.id", Matchers.is(1)),
                        jsonPath("$.name", Matchers.is("test1"))
                );
        ///getting non-existing customer///-----------------------------------
        mockMvc.perform(
                        get("/customers/2").
                                accept(MediaType.APPLICATION_JSON)).
                andExpect(status().isNotFound()).
                andExpect(
                        jsonPath("$.message", Matchers.is("Entity ID was not found"))
                );
        Mockito.verify(customerService,
                Mockito.times(1)).readById(1L);
        Mockito.verify(customerService,
                Mockito.times(1)).readById(1L);
    }

    @Test
    void createCustomer() throws Exception {
        ///success///------------------------------------------
        Assertions.assertThrows(
                ///controller will throw exception, because mocking service will return null
                ///since there is no way how to mock create method
                NestedServletException.class, ()->
                mockMvc.perform(
                post("/customers").
                        contentType(MediaType.APPLICATION_JSON).
                        content("{" +
                                "\"name\": \"test1\"," +
                                "\"email\": \"demo_email@mail.com\"" +
                                "}")
                ).andExpect(status().isCreated())
        );
        //no sense to test what was returned since it's unit test//
        ///wrong format of json (email is necessary)///---------
        mockMvc.perform(
                        post("/customers").
                                contentType(MediaType.APPLICATION_JSON).
                                content("{" +
                                        "\"name\": \"test1\"," +
                                        "}")).
                andExpect(status().isBadRequest());
    }

    @Test
    void deleteCustomer() throws Exception {
        ///somehow customerService.delete(...) is not method
        var f = customerService.getClass().getMethod("delete", Long.class);
        Mockito.when(f.invoke(customerService,2L)).
                thenThrow(new EntityNotFoundEXception("Entity with given ID does not exist")
                );
        ///deleting existing customer///---------------------------
        mockMvc.perform(
                        delete("/customers/1").
                                accept(MediaType.APPLICATION_JSON)).
                andExpect(status().isOk());


        ///deleting non-existing customer///------------------------
        mockMvc.perform(
                        delete("/customers/2").
                                accept(MediaType.APPLICATION_JSON)).
                andExpect(status().isNotFound());
    }
}