package com.food_service.server.business;

import com.food_service.server.dao.EntityNotFoundEXception;
import com.food_service.server.dao.RestaurantJpaRepository;
import com.food_service.server.domain.Restaurant;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestaurantServiceTest {
    @Autowired
    private RestaurantService restaurantService;

    @MockBean
    private RestaurantJpaRepository repository;

    Restaurant existing = new Restaurant(1L, "test1", Restaurant.RestaurantType.BURGER);
    Restaurant notExist = new Restaurant(2L, "test2", Restaurant.RestaurantType.BURGER);

    @BeforeEach
    void setUp() {
        /*
            mocking RestaurantRepository class
        */
        Mockito.when(repository.existsById(existing.getId())).thenReturn(true);
        Mockito.when(repository.existsById(notExist.getId())).thenReturn(false);
        Mockito.when(repository.findById(existing.getId())).
                thenReturn(Optional.ofNullable(existing));
        Mockito.when(repository.findById(notExist.getId())).
                thenReturn(Optional.empty());
    }

    @Test
    void createRestaurant() {
        //creating restaurant// --------------------------------------------------
        Mockito.when(repository.save(notExist)).thenReturn(notExist);
        Assertions.assertDoesNotThrow(
                () -> {
                    Restaurant created = restaurantService.create(notExist);
                    Assertions.assertEquals(created, notExist);
                }
        );
        Mockito.verify(repository, Mockito.times(1)).save(notExist);
        //creating the already existing one// -------------------------
        Assertions.assertThrows(
                EntityNotFoundEXception.class,
                () -> restaurantService.create(existing)
        );
        Mockito.verify(repository, Mockito.times(0)).save(existing);
    }

    @Test
    void getById(){
        //get method returns the same restaurant// ----------------------
        Assertions.assertEquals(
                Assertions.assertDoesNotThrow(
                        ()-> restaurantService.readById(existing.getId())
                )
                , existing
        );
        Mockito.verify(repository, Mockito.times(1)).findById(existing.getId());
        //try to get not existing restaurant// -----------------------------------
        Assertions.assertThrows(
                EntityNotFoundEXception.class,
                () -> restaurantService.readById(notExist.getId())
        );
        Mockito.verify(repository, Mockito.times(1)).findById(notExist.getId());
    }

    @Test
    void delete() {
        //delete non-existing restaurant// ----------------------
        Assertions.assertThrows(
                EntityNotFoundEXception.class,
                () -> restaurantService.delete(notExist.getId())
        );
        Mockito.verify(repository, Mockito.never()).deleteById(notExist.getId());
        //delete existing restaurant// ------------------------------------
        Assertions.assertDoesNotThrow(
                () -> restaurantService.delete(existing.getId())
        );
        Mockito.verify(repository, Mockito.times(1)).deleteById(existing.getId());
        //-------------------------------------------------------
    }
}
