package com.food_service.server.business;

import com.food_service.server.dao.EntityNotFoundEXception;
import com.food_service.server.dao.MenuItemJpaRepository;
import com.food_service.server.domain.MenuItem;
import com.food_service.server.domain.Restaurant;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MenuItemServiceTest {

    @Autowired
    private MenuItemService menuItemService;

    @MockBean
    private MenuItemJpaRepository repository;

    Restaurant restaurant = new Restaurant(1L, "test1", Restaurant.RestaurantType.BURGER);
    MenuItem existing = new MenuItem(1, restaurant, "test1", 200);
    MenuItem existing2 = new MenuItem(3, restaurant, "test3", 200);
    MenuItem notExist = new MenuItem(2, restaurant, "test1", 100);

    @BeforeEach
    void setUp() {
        /*
            mocking MenuItemRepository class
        */
        Mockito.when(repository.existsById(existing.getId())).thenReturn(true);
        Mockito.when(repository.existsById(existing2.getId())).thenReturn(true);
        Mockito.when(repository.existsById(notExist.getId())).thenReturn(false);
        Mockito.when(repository.findById(existing.getId())).
                thenReturn(Optional.ofNullable(existing));
        Mockito.when(repository.findById(notExist.getId())).
                thenReturn(Optional.empty());
        Mockito.when(repository.findAllById(List.of(1,2))).
                thenReturn(List.of(existing,notExist));

        Mockito.when(repository.findById(existing.getId())).thenReturn(Optional.of(existing));
        Mockito.when(repository.findById(existing2.getId())).thenReturn(Optional.of(existing2));
        Mockito.when(repository.findAllById(List.of(existing.getId(), existing2.getId()))).
                thenReturn(List.of(existing, existing2));
    }
    @Test
    void createItem() {
        Mockito.when(repository.save(notExist)).thenReturn((notExist));
        //creating item// --------------------------------------------------
        Assertions.assertDoesNotThrow(
                () -> {
                    MenuItem created = menuItemService.create(notExist);
                    Assertions.assertEquals(created, notExist);
                }
        );
        Mockito.verify(repository, Mockito.times(1)).save(notExist);
        //creating the already existing one// -------------------------
        Assertions.assertThrows(
                EntityNotFoundEXception.class,
                () -> menuItemService.create(existing)
        );
        Mockito.verify(repository, Mockito.times(0)).save(existing);
    }

    @Test
    void getById(){
        //get method returns the same item// ----------------------
        Assertions.assertEquals(
                Assertions.assertDoesNotThrow(
                        ()-> menuItemService.readById(existing.getId())
                )
                , existing
        );
        Mockito.verify(repository, Mockito.times(1)).findById(existing.getId());
        //try to get non existing item// -----------------------------------
        Assertions.assertThrows(
                EntityNotFoundEXception.class,
                () -> menuItemService.readById(notExist.getId())
        );
        Mockito.verify(repository, Mockito.times(1)).findById(notExist.getId());
    }
    @Test
    void delete() {
        //delete non-existing item// ----------------------
        Assertions.assertThrows(
                EntityNotFoundEXception.class,
                () -> menuItemService.delete(notExist.getId())
        );
        Mockito.verify(repository, Mockito.never()).deleteById(notExist.getId());
        //delete existing item// ------------------------------------
        Assertions.assertDoesNotThrow(
                () -> menuItemService.delete(existing.getId())
        );
        Mockito.verify(repository, Mockito.times(1)).deleteById(existing.getId());
        //-------------------------------------------------------
    }

    @Test
    void getItems() {
        /**
         * method should not really be tested since it uses only 1 repository method
         */
        List<Integer> ids = List.of(existing.getId(), existing2.getId());
        var items = List.of(existing, existing2);

        assertEquals(
                Assertions.assertDoesNotThrow(
                        ()-> menuItemService.getItems(ids)
                ),
                items
        );
        //-----------------------------------------------------------------
        Mockito.verify(repository, Mockito.times(1)).
                findAllById(ids);
    }
}