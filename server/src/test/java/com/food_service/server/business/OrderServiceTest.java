package com.food_service.server.business;

import com.food_service.server.dao.EntityNotFoundEXception;
import com.food_service.server.dao.OrderJpaRepository;
import com.food_service.server.domain.Customer;
import com.food_service.server.domain.MenuItem;
import com.food_service.server.domain.Order;
import com.food_service.server.domain.Restaurant;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderServiceTest {
    @Autowired
    private OrderService orderService;

    @MockBean
    private OrderJpaRepository repository;

    Restaurant restaurant = new Restaurant(1L, "test1", Restaurant.RestaurantType.BURGER);
    Restaurant restaurant1 = new Restaurant(2L, "test2", Restaurant.RestaurantType.BURGER);

    Customer customer = new Customer(1L, "test1", null, null, null);

    MenuItem item1 = new MenuItem(1, restaurant, "item1", 100);
    MenuItem item2 = new MenuItem(2, restaurant, "item1", 200);
    MenuItem item3 = new MenuItem(3, restaurant1, "item1", 200);

    Order existing = new Order(1L, customer,
            restaurant, List.of(item1,item2));
    Order notExist = new Order (2L, customer, restaurant, List.of(item1,item2));
    Order wrongOrder = new Order (3L, customer, restaurant, List.of(item1, item2, item3));

    @BeforeEach
    void setUp() {
        /*
            mocking OrderRepository class
        */
        Mockito.when(repository.existsById(existing.getId())).thenReturn(true);
        Mockito.when(repository.existsById(notExist.getId())).thenReturn(false);
        Mockito.when(repository.findById(existing.getId())).
                thenReturn(Optional.ofNullable(existing));
        Mockito.when(repository.findById(notExist.getId())).
                thenReturn(Optional.empty());
        //---------------------------------------------------
        Mockito.when(repository.existsById(wrongOrder.getId()))
                .thenReturn(false);
    }

    @Test
    void createOrder() {
        //creating order// --------------------------------------------------
        Mockito.when(repository.save(notExist)).thenReturn(notExist);
        Assertions.assertDoesNotThrow(
                () -> {
                    Order created = orderService.create(notExist);
                    Assertions.assertEquals(notExist, created);
                }
        );
        Mockito.verify(repository, Mockito.times(1)).save(notExist);
        //creating the already existing one// -------------------------
        Assertions.assertThrows(
                EntityNotFoundEXception.class,
                () -> orderService.create(existing)
        );
        Mockito.verify(repository, Mockito.times(0)).save(existing);
        /**
         * creating order with items from different restaurants:
         * - throws exception
         * - does not save this order
         */
        Assertions.assertThrows(
                EntityNotFoundEXception.class,
                ()-> orderService.create(wrongOrder)
        );
        Mockito.verify(repository, Mockito.times(1)).
                existsById(wrongOrder.getId());
        Mockito.verify(repository, Mockito.never()).save(wrongOrder);
    }

    @Test
    void getById(){
        //get method returns the same order// ----------------------
        Assertions.assertEquals(
                Assertions.assertDoesNotThrow(
                        ()-> orderService.readById(existing.getId())
                )
                , existing
        );
        Mockito.verify(repository, Mockito.times(1)).findById(existing.getId());
        //try to get not existing order// -----------------------------------
        Assertions.assertThrows(
                EntityNotFoundEXception.class,
                () -> orderService.readById(notExist.getId())
        );
        Mockito.verify(repository, Mockito.times(1)).findById(notExist.getId());
    }

    @Test
    void delete() {
        //delete non-existing order// ----------------------
        Assertions.assertThrows(
                EntityNotFoundEXception.class,
                () -> orderService.delete(notExist.getId())
        );
        Mockito.verify(repository, Mockito.never()).deleteById(notExist.getId());
        //delete existing order// ------------------------------------
        Assertions.assertDoesNotThrow(
                () -> orderService.delete(existing.getId())
        );
        Mockito.verify(repository, Mockito.times(1)).deleteById(existing.getId());
        //-------------------------------------------------------
    }
}
