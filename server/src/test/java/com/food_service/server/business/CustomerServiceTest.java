package com.food_service.server.business;

import com.food_service.server.dao.CustomerJpaRepository;
import com.food_service.server.dao.EntityNotFoundEXception;
import com.food_service.server.domain.Customer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerServiceTest {
    @Autowired
    private CustomerService customerService;

    @MockBean
    private CustomerJpaRepository repository;

    Customer existingCustomer = new Customer(1, "test1", null, null, null);
    Customer notExist = new Customer (2, "test2", null, null, null);

    @BeforeEach
    void setUp() {
        /*
            mocking CustomerRepository class
        */
        Mockito.when(repository.existsById(existingCustomer.getId())).thenReturn(true);
        Mockito.when(repository.existsById(notExist.getId())).thenReturn(false);
        Mockito.when(repository.findById(existingCustomer.getId())).
                thenReturn(Optional.ofNullable(existingCustomer));
        Mockito.when(repository.findById(notExist.getId())).
                thenReturn(Optional.empty());
    }
    @Test
    void createCustomer() {
        //inserting customer// --------------------------------------------------
        Mockito.when(repository.save(notExist)).thenReturn(notExist);
        Assertions.assertDoesNotThrow(
                () -> {
                    var created = customerService.create(notExist);
                    Assertions.assertEquals(created, notExist);
                }
        );
        Mockito.verify(repository, Mockito.times(1)).save(notExist);
        //inserting the already existing one// -------------------------
        Assertions.assertThrows(
                EntityNotFoundEXception.class,
                () -> customerService.create(existingCustomer)
        );
        Mockito.verify(repository, Mockito.times(0)).save(existingCustomer);
    }

    @Test
    void getById(){
        //get method returns the same customer// ----------------------
        Assertions.assertEquals(
                Assertions.assertDoesNotThrow(
                        ()-> customerService.readById(existingCustomer.getId())
                )
                , existingCustomer
        );
        Mockito.verify(repository, Mockito.times(1)).findById(existingCustomer.getId());
        //get non existing customer// -----------------------------------
        Assertions.assertThrows(
                EntityNotFoundEXception.class,
                () -> customerService.readById(notExist.getId())
        );
        Mockito.verify(repository, Mockito.times(1)).findById(notExist.getId());
    }
    @Test
    void delete() {
        //delete non-existing customer// ----------------------
        Assertions.assertThrows(
                EntityNotFoundEXception.class,
                () -> customerService.delete(notExist.getId())
        );
        Mockito.verify(repository, Mockito.never()).deleteById(notExist.getId());
        //delete existing customer// ------------------------------------
        Assertions.assertDoesNotThrow(
                () -> customerService.delete(existingCustomer.getId())
        );
        Mockito.verify(repository, Mockito.times(1)).deleteById(existingCustomer.getId());
        //-------------------------------------------------------
    }

}
