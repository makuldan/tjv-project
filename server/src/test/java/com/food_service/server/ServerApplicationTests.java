package com.food_service.server;

import com.food_service.server.business.CustomerServiceTest;
import com.food_service.server.business.MenuItemServiceTest;
import com.food_service.server.business.OrderServiceTest;
import com.food_service.server.business.RestaurantServiceTest;
import com.food_service.server.controllers.*;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.springframework.boot.test.context.SpringBootTest;


/**
 * testy mely by byt pripojeny k jine databazi nez k te, kterou pouziva aplikace
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({
		CustomerServiceTest.class,
		MenuItemServiceTest.class,
		OrderServiceTest.class,
		RestaurantServiceTest.class,
		RestaurantControllerTest.class,
		CustomerControllerTest.class,
		OrderControllerTest.class,
		MenuItemControllerTest.class
})
@SpringBootTest
class ServerApplicationTests {

	@Test
	void contextLoads() {

	}

}
