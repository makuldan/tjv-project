# Food Delivery Server-Client Application 
semestral project written in Java using Spring Framework 
## Table of Contents 
1. [General Info](#general-info)
2. [Description](#description) 
3. [Technologies](#technologies)
4. [Installation](#installation-and-running)
5. [Client App](#client)
6. [Tests](#running-tests)

## General info

* all application consists of 2 parts: server and client

* server represents back-end for simple Food Delivery Service (watch [Description](#description))

* client is Shell based application that allows customer to sign in/up and perform basic operations

## Description

* 4 main entites: Customer, Order, Restaurant and MenuItem
* Customer makes an Order from a Restaurant
* each Order has 1-n items from a certain Restaurant
* each Item is in menu of 1 Restaurant

## Technologies  

* server written using Java, Spring Framework, gradle 

* Postgres used in Data Layer

* provided docker-compose for Data Layer (no need for configuring from elsewhere)

* gitlab CI/CD provided for both server and client

* client written using Java, Spring Framework, gradle 

## Installation and Running

### Running whole application (client + server)

```bash
  #not yet implemented
  $ ./build.sh
  $ ./run.sh
```

### Installing Database 
Needed: 
* docker
* docker-compose

```bash
  $ Database/docker-compose up -d
```
for using docker or docker-compose, watch 
```Database/dockerCheatSheet```

### Installing Server

Needed: 
* JDK version 16+
* $JAVA_HOME variable set

```bash
  $ server/gradlew build
  $ server/gradlew bootRun
```

### Installing Client

Needed:
* JDK version 16+
* $JAVA_HOME variable set

```bash
  $ client/buildRun
```

## Running Tests

To run tests, run the following command

```bash
  $ server/gradlew cleanTest test
```

tests are provided for all services (business logic)
and controllers (REST API)

## Client

### Interactive Shell application

***
application allows user to sign up new account or to sign in to already existing customer account using his ID and email address

registered customers can perform basic operations in his/her account, such as 
* check out their order history
* change account
* delete it
***
admin role (can be set using 'set-admin' command) allows more options:
* get and print general info of all customers
* get and print all info of single customer
* get and print all orders
* delete customer by his id
***
for command syntax and full command guide, use 'help' command