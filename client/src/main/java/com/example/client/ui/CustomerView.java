package com.example.client.ui;

import com.example.client.model.CustomerDto;
import org.springframework.shell.ParameterMissingResolutionException;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClientException;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.util.Collection;
import java.util.List;

@Component
public class CustomerView {
    public void printCustomers(Collection<CustomerDto> customers) {
        System.out.printf("%-4s%-15s%-12s%-20s\n", "ID", "Name", "Number", "Email");
        customers.forEach(customer ->
                System.out.printf("%-4d%-15s%-12s%-20s\n", customer.getId(), customer.getName(),
                        customer.getNumber(), customer.getEmail())
        );
    }

    public void printCustomer(CustomerDto customer) {
        System.out.println("customer " + customer.getId());
        System.out.printf("   name is \"%s\"\n", customer.getName());
        System.out.printf("   email is \"%s\"\n", customer.getEmail());
        System.out.printf("   number is %s\n", customer.getNumber());
        if (customer.getAddress() != null)
            System.out.printf("   address is %s\n",customer.getAddress());
        else System.out.printf("   address not set\n");
        if (customer.getOrderIds().size()!=0) {
            System.out.printf("   orders are ");
            printOrders(customer.getOrderIds());
            printMessage("\n*for detailed order information, use list-orders command*");
        } else printMessage("   no orders made yet");
    }

    private void printOrders(List<Long> orderIds) {
        System.out.print("[");
        for (int i = 0; i < orderIds.size(); i++) {
            System.out.print(orderIds.get(i));
            if (i != orderIds.size()-1)
                System.out.print(",");
        }
        System.out.print("]");
    }

    public void printErrorGeneric(Exception e) {
        if (e instanceof WebClientException) {
            System.err.println("Can not connect to server");
        }
    }

    public void printError400(WebClientResponseException e) {
        if (e instanceof WebClientResponseException.NotFound) {
            System.err.println("Customer with given ID does not exist");
        } else printErrorGeneric(e);
    }

    public void printErrorMismatch(ParameterMissingResolutionException e) {
        System.err.println(e.getMessage());
    }

    public void printMessage(String message) {
        System.out.println(message);
    }
}