package com.example.client.ui;

import com.example.client.data.CustomerClient;
import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStyle;
import org.springframework.shell.jline.PromptProvider;
import org.springframework.stereotype.Component;

@Component
public class ClientPromptProvider implements PromptProvider {
    private final CustomerClient customerClient;
    private final String adminName = "ADMIN";
    private final AttributedString adminPrompt;
    private final AttributedString defaultPrompt;


    public ClientPromptProvider(CustomerClient customerClient) {
        this.customerClient = customerClient;
        adminPrompt = new AttributedString(
                "[" + adminName+ "]> ",
                AttributedStyle.BOLD.foreground(AttributedStyle.RED)
        );
        defaultPrompt = new AttributedString(
                "guest:> ",
                AttributedStyle.DEFAULT.foreground(AttributedStyle.BRIGHT)
        );
    }

    @Override
    public AttributedString getPrompt() {
        if (customerClient.isAdminSet()) return adminPrompt;
        else if (customerClient.isCustomerSet()) {
            return new AttributedString(
                    "[customer " + customerClient.getCurCustomer().getName() + "]> ",
                    AttributedStyle.DEFAULT.foreground(AttributedStyle.GREEN)
            );
        }
        else return defaultPrompt;
    }
}
