package com.example.client.ui;

import com.example.client.model.OrderDto;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

@Component
public class OrderView {

    public void printOrders(Collection<OrderDto> orders) {
        orders.forEach(this::printOrder);
    }

    public void printOrder(OrderDto order) {
        System.out.printf("order %d:\n", order.getId());
        System.out.printf("   status is %s\n", order.getStatus());
        System.out.printf("   date is ");
        printDate(order.getOrderDate());
        System.out.printf("   made by customer %d\n", order.getCustomerId());
        System.out.printf("   made from restaurant %d\n", order.getRestaurantId());
        System.out.printf("   items are ");
        printItems(order.getItemIds());
        System.out.println();
    }

    private void printItems(List<Integer> itemIds) {
        System.out.print("[");
        for (int i = 0; i < itemIds.size(); i++) {
            System.out.print(itemIds.get(i));
            if (i != itemIds.size()-1)
                System.out.print(",");
        }
        System.out.print("]");
    }

    private void printDate(LocalDateTime date){
        System.out.printf("%02d.%02d.%4d at %2d:%2d\n",
                date.getDayOfMonth(),
                date.getMonth().getValue(),
                date.getYear(),
                date.getHour(),
                date.getMinute()
        );
    }
}
