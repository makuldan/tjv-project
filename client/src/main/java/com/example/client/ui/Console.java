package com.example.client.ui;

import com.example.client.data.CustomerClient;
import com.example.client.data.OrderClient;
import com.example.client.model.CustomerDto;
import org.springframework.shell.Availability;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellMethodAvailability;
import org.springframework.shell.standard.ShellOption;
import org.springframework.web.reactive.function.client.WebClientException;
import org.springframework.web.reactive.function.client.WebClientResponseException;

@ShellComponent
public class Console {
    private final CustomerClient customerClient;
    private final OrderClient orderClient;
    private final CustomerView customerView;
    private final OrderView orderView;

    public Console(CustomerClient customerClient, OrderClient orderClient, CustomerView customerView, OrderView orderView) {
        this.orderClient = orderClient;
        this.customerView = customerView;
        this.customerClient = customerClient;
        this.orderView = orderView;
    }
    ///=================================================================
    ///methods available for unregistered customers
    @ShellMethod("Create new customer")
    @ShellMethodAvailability("curCustomerNotSet")
    public void createCustomer(String email, String name, String number) {
        CustomerDto customer = new CustomerDto();
        customer.setEmail(email);
        customer.setName(name);
        customer.setNumber(number);
        CustomerDto created = customerClient.create(customer);
        customerView.printCustomer(created);
        setCustomer(created.getId(), created.getEmail());
    }

    ///==================================================================
    ///ADMIN methods
    @ShellMethod("sets current customer as admin, admin has more rights")
    public void setAdmin() {
        customerClient.setAdmin();
    }

    @ShellMethod("unset admin")
    @ShellMethodAvailability("isAdminSet")
    public void unsetAdmin() {
        customerClient.unsetAdmin();
    }

    @ShellMethod("Print single customer by his ID")
    @ShellMethodAvailability("isAdminSet")
    public void listCustomer(@ShellOption(defaultValue = "1L")long customerId) {
        try {
            customerView.printCustomer(customerClient.getCustomer(customerId));
        } catch (WebClientResponseException e) {
            customerView.printError400(e);
        }
    }

    @ShellMethod("Retrieve and display all customers")
    @ShellMethodAvailability("isAdminSet")
    public void listAllCustomers(){
        try {
            var all = customerClient.getAll();
            customerView.printCustomers(all);
        } catch (WebClientException e) {
            customerView.printErrorGeneric(e);
        }
    }

    @ShellMethod("Retrieve and display all orders")
    @ShellMethodAvailability("isAdminSet")
    public void listAllOrders() {
        orderView.printOrders(orderClient.getAll());
    }

    @ShellMethod("delete customer with given ID")
    @ShellMethodAvailability("isAdminSet")
    public void deleteCustomer(long customerId) {
        try {
            customerClient.delete(customerId);
        } catch (WebClientResponseException e) {
            customerView.printError400(e);
        }
    }

    @ShellMethod("Get all orders made by customer with given ID")
    @ShellMethodAvailability("isAdminSet")
    public void listOrdersMadeBy(long customerId){
        var orders = orderClient.getAllBy(customerId);
        orderView.printOrders(orders);
    }
    ///=========================================================
    ///methods for single customer
    @ShellMethod("sets current customer by id and email")
    @ShellMethodAvailability("currentCustomerNotSet")
    public void setCustomer(long customerId, String email) {
        if (!customerClient.setCurCustomer(customerId, email))
            customerView.printMessage("email mismatch");
    }

    @ShellMethod("gets info on current customer")
    @ShellMethodAvailability("currentCustomerSet")
    public void getCurrent(){
        customerView.printMessage("Current customer:");
        customerView.printCustomer(customerClient.getCurCustomer());
    }

    @ShellMethod("unsets current customer")
    @ShellMethodAvailability("currentCustomerSet")
    public void unsetCustomer() {
        customerClient.unsetCustomer();
    }

    @ShellMethod("Update name of current customer")
    @ShellMethodAvailability("currentCustomerSet")
    public void updateName(String name) {
        CustomerDto updated = customerClient.updateCustomerName(customerClient.getCurCustomer().getId(), name);
        customerClient.setCurCustomer(updated);
    }

    @ShellMethod("Get all orders made by current customer")
    @ShellMethodAvailability("currentCustomerSet")
    public void listOrders(){
        var orders = orderClient.getAllBy(customerClient.getCurCustomer().getId());
        orderView.printOrders(orders);
    }

    @ShellMethod("delete current customer")
    @ShellMethodAvailability("currentCustomerSet")
    public void deleteCurrent() {
        customerClient.delete(customerClient.getCurCustomer().getId());
        customerClient.unsetCustomer();
    }

    ///====================================================================
    ///Availability methods
    private Availability currentCustomerNotSet(){
        if (customerClient.isAdminSet()) return Availability.unavailable("admin is not customer");
        return customerClient.isCustomerSet() ?
                Availability.unavailable("you need to sign out first") :
                Availability.available();
    }

    private Availability currentCustomerSet(){
        if (customerClient.isAdminSet()) return Availability.unavailable("admin is not customer");
        return customerClient.isCustomerSet() ?
                Availability.available() :
                Availability.unavailable("current customer is not set");
    }

    private Availability isAdminSet() {
        return customerClient.isAdminSet() ?
                Availability.available() :
                Availability.unavailable("only admin can use this method");
    }
}
