package com.example.client.model;
import java.util.ArrayList;
import java.util.List;

public class CustomerDto {
    private Long id;
    private String name;
    private String address;
    private String email;
    private String number;
    private List<Long> orderIds;

    public CustomerDto() {
        /*
          used by spring for creating CustomerDto objects
         */
        id = -1L;
        orderIds = new ArrayList<>();
    }

    public CustomerDto(long id, String name, String address, String email, String number, List<Long> orderIds) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.email = email;
        this.number = number;
        this.orderIds = orderIds;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public List<Long> getOrderIds() {
        return orderIds;
    }

    public void setOrderIds(List<Long> orderIds) {
        this.orderIds = orderIds;
    }
}
