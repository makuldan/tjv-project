package com.example.client.data;

import com.example.client.model.OrderDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Collection;

@Component
public class OrderClient {
    private final WebClient orderWebClient;

    public OrderClient(@Value("${backend_url}") String backendUrl) {
        orderWebClient = WebClient.create(backendUrl + "/orders");
    }

    public Collection<OrderDto> getAll() {
        return orderWebClient.get().
                accept(MediaType.APPLICATION_JSON).
                retrieve().
                bodyToFlux(OrderDto.class).
                collectList().
                block();
    }

    public Collection<OrderDto> getAllBy(long customerId) {
        return orderWebClient.get().uri("/by/{id}", customerId).
                accept(MediaType.APPLICATION_JSON).
                retrieve().
                bodyToFlux(OrderDto.class).
                collectList().
                block();
    }
}
