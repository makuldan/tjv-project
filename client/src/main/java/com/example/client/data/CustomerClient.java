package com.example.client.data;

import com.example.client.model.CustomerDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Collection;

@Component
public class CustomerClient {
    private final WebClient customerWebClient;
    private CustomerDto curCustomer = null;
    private static CustomerDto admin = new CustomerDto(0L, "admin", null, null, null, null);

    public CustomerClient(@Value("${backend_url}") String backendUrl) {
        customerWebClient = WebClient.create(backendUrl + "/customers");
    }

    public CustomerDto create(CustomerDto customerDto){
        return customerWebClient.post().
                contentType(MediaType.APPLICATION_JSON).
                accept(MediaType.APPLICATION_JSON).
                bodyValue(customerDto).
                retrieve().
                bodyToMono(CustomerDto.class).
                block();
    }

    public Collection<CustomerDto> getAll() {
        return customerWebClient.get().
                accept(MediaType.APPLICATION_JSON).
                retrieve().
                ///0 - n objects
                bodyToFlux(CustomerDto.class).
                collectList().
                ///get all n objects, then close connection
                block();
    }

    public CustomerDto getCustomer(long customerId) {
        return customerWebClient.get().uri("/" + customerId).
                accept(MediaType.APPLICATION_JSON).
                retrieve().
                bodyToMono(CustomerDto.class).
                block();
    }

    public CustomerDto updateCustomerName(long customerId, String newName){
        return customerWebClient.patch().
                uri("/{id}", customerId).
                contentType(MediaType.valueOf("application/json-patch+json")).
                accept(MediaType.APPLICATION_JSON).
                bodyValue("[ {\"op\":\"replace\",\"path\":\"/name\",\"value\":" +
                        "\"" + newName + "\"}]").
                retrieve().
                bodyToMono(CustomerDto.class).
                block();
    }

    public void delete(long customerId) {
        customerWebClient.delete().uri("/{id}", customerId).
                retrieve().
                toBodilessEntity().
                subscribe(x -> {});
    }

    public CustomerDto getCurCustomer() {
        return curCustomer;
    }

    public boolean setCurCustomer(long customerId, String email) {
        CustomerDto customer = getCustomer(customerId);
        if (!customer.getEmail().equals(email))
            return false;
        this.curCustomer = customer;
        return true;
    }

    public void setCurCustomer(CustomerDto customer) {
        curCustomer = customer;
    }

    public void unsetCustomer() {
        if (isAdminSet()) return;
        curCustomer = null;
    }
    public boolean isCustomerSet() {
        if (isAdminSet()) return false;
        return curCustomer != null;
    }
    public void setAdmin(){
        curCustomer = admin;
    }
    public void unsetAdmin() {
        if (curCustomer != admin) throw new IllegalStateException("admin not set");
        curCustomer = null;
    }
    public boolean isAdminSet() {
        return curCustomer == admin;
    }
}
