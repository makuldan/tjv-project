#!/bin/bash

if [ ${JAVA_HOME} -eq "" ]
then 
    echo 'JAVA_HOME variable not set'
    exit
fi

#installing docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

#creating database
Database/docker-compose up -d

#CONTAINER_NAME=$(docker ps -a sudo docker ps -a | grep 5432 | awk '{print $NF}')

#building server
/server/gradlew bootRun

#

